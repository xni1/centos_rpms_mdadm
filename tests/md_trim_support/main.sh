#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /tests/kernel/storage/mdadm/trim-support
#   Description: test the function trim support.
#   Author: Xiao Ni <xni@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2011 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#set -x
# Include Beaker environment
. /usr/bin/rhts-environment.sh
. /usr/lib/beakerlib/beakerlib.sh

# Include Storage related environment
. include.sh || exit 200

function Stop_Raid (){
	tok mdadm --stop "$MD_RAID"
	if [ $? -ne 0 ]; then
		tlog "FAIL: fail to stop md raid $MD_RAID."
		exit 1
	fi
}

function runtest (){
	
#	tok rmmod raid456
	tok modprobe raid456 devices_handle_discard_safely=Y
	tok "echo Y >/sys/module/raid456/parameters/devices_handle_discard_safely"
#	info=`cat /etc/redhat-release | grep -oE "7.2|7.3"`
#	if [ -n "$info" ]; then
		trun "modprobe raid0 devices_discard_performance=Y"
		trun "echo Y >/sys/module/raid0/parameters/devices_discard_performance"
#	fi
	devlist=''

	which mkfs.xfs
	if [ $? -eq 0 ]; then
		FILESYS="xfs"
	else
		FILESYS="ext4"
	fi

	disk_num=6
	#disk size M
	disk_size=500
	get_disks $disk_num $disk_size
	devlist=$RETURN_STR

	if [ $JOURNAL_SUPPORT -eq 1 ]; then
		RAID_LIST="0 1 4 5 6 4-j 5-j 6-j 10"
	else
		RAID_LIST="0 1 4 5 6 10"
	fi
	for level in $RAID_LIST; do

		RETURN_STR=''
		MD_RAID=''
		MD_DEV_LIST=''
		raid_num=5
		if [ "$level" = "0" ];then
			spare_num=0
			bitmap=0
		else
			spare_num=1
			bitmap=1
		fi

		if [[ $level =~ "j" ]]; then
			MD_Create_RAID_Journal ${level:0:1} "$devlist" $raid_num $bitmap $spare_num
		else
			MD_Create_RAID $level "$devlist" $raid_num $bitmap $spare_num
		fi

		if [ $? -ne 0 ];then
			tlog "FAIL: Failed to create md raid $RETURN_STR"
			break
		else
			tlog "INFO: Successfully created md raid $RETURN_STR"
		fi

		MD_RAID=$RETURN_STR
	
		MD_Get_State_RAID $MD_RAID
		state=$RETURN_STR
		
		while [[ $state != "active" && $state != "clean" ]]; do
			sleep 5
			MD_Get_State_RAID $MD_RAID
			state=$RETURN_STR
		done
	
		tlog "mkfs -t $FILESYS $MD_RAID"
		(mkfs -t $FILESYS $MD_RAID) || (mkfs -t $FILESYS -f $MD_RAID)
		if [ ! -d /mnt/md_test ]; then
			mkdir /mnt/md_test
		fi
		tok mount -t $FILESYS $MD_RAID /mnt/md_test 
		tok fstrim -v /mnt/md_test
		if [ $? -ne 0 ];then
			tlog "fstrim -v /mnt/md_test failed"	
		fi
	
		tok umount $MD_RAID
		MD_Clean_RAID $MD_RAID
	
	done	
	
	remove_disks "$devlist"
	if [ $? -ne 0 ];then
		exit 1
	fi
}


tlog "running $0"
trun "rpm -q mdadm || yum install -y mdadm"
trun "uname -a"
runtest

tend
