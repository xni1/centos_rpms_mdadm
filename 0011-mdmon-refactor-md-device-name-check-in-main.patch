From c8772da4b53307546a9a374507bcec3398fc82c4 Mon Sep 17 00:00:00 2001
From: Mateusz Kusiak <mateusz.kusiak@intel.com>
Date: Tue, 20 Feb 2024 11:56:12 +0100
Subject: [PATCH 11/41] mdmon: refactor md device name check in main()

Refactor mdmon main function to verify if fd is valid prior to checking
device name. This is due to static code analysis complaining after
change b938519e7719 ("util: remove obsolete code from get_md_name").

Signed-off-by: Mateusz Kusiak <mateusz.kusiak@intel.com>
Signed-off-by: Mariusz Tkaczyk <mariusz.tkaczyk@linux.intel.com>
---
 mdmon.c | 21 +++++++++++----------
 1 file changed, 11 insertions(+), 10 deletions(-)

diff --git a/mdmon.c b/mdmon.c
index a2038fe6..5fdb5cdb 100644
--- a/mdmon.c
+++ b/mdmon.c
@@ -302,12 +302,12 @@ static int mdmon(char *devnm, int must_fork, int takeover);
 int main(int argc, char *argv[])
 {
 	char *container_name = NULL;
-	char *devnm = NULL;
 	int status = 0;
 	int opt;
 	int all = 0;
 	int takeover = 0;
 	int dofork = 1;
+	int mdfd = -1;
 	bool help = false;
 	static struct option options[] = {
 		{"all", 0, NULL, 'a'},
@@ -410,19 +410,20 @@ int main(int argc, char *argv[])
 		free_mdstat(mdstat);
 
 		return status;
-	} else {
-		int mdfd = open_mddev(container_name, 0);
-		devnm = fd2devnm(mdfd);
+	}
+
+	mdfd = open_mddev(container_name, 0);
+	if (is_fd_valid(mdfd)) {
+		char *devnm = fd2devnm(mdfd);
 
 		close(mdfd);
-	}
 
-	if (!devnm) {
-		pr_err("%s is not a valid md device name\n",
-			container_name);
-		return 1;
+		if (devnm)
+			return mdmon(devnm, dofork && do_fork(), takeover);
 	}
-	return mdmon(devnm, dofork && do_fork(), takeover);
+
+	pr_err("%s is not a valid md device name\n", container_name);
+	return 1;
 }
 
 static int mdmon(char *devnm, int must_fork, int takeover)
-- 
2.40.1

